package com.develop.servicios.service;

import com.develop.servicios.Document.ArrendatarioM;
import com.develop.servicios.repository.ArrendatarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ArrendatarioServiceImp implements ArrendatarioService{

    @Autowired
    private ArrendatarioRepository arrendatarioRepository; //Inyectando lo de ArrendatarioRepository

    @Override
    @Transactional(readOnly = true) //Solo buscar
    public Iterable<ArrendatarioM> findAll() {
        return arrendatarioRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ArrendatarioM> findAll(Pageable pageable) {
        return arrendatarioRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ArrendatarioM> findById(Long id) {
        return arrendatarioRepository.findById(id);
    }

    @Override
    public ArrendatarioM save(ArrendatarioM arrendatarioM) {
        return arrendatarioRepository.save(arrendatarioM);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        arrendatarioRepository.deleteById(id);
    }
}
