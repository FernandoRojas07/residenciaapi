package com.develop.servicios.service;

import com.develop.servicios.Document.ArrendatarioM;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ArrendatarioService {
    public Iterable<ArrendatarioM> findAll();
    public Page<ArrendatarioM> findAll(Pageable pageable);
    public Optional<ArrendatarioM> findById(Long id);
    public ArrendatarioM save(ArrendatarioM arrendatarioM);
    public void deleteById(Long id);

}
