package com.develop.servicios.repository;

import com.develop.servicios.Document.ArrendatarioM;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.lang.*;

@Repository
public interface ArrendatarioRepository extends MongoRepository<ArrendatarioM, Long> {
    @Query("{_id:'?0'}")
    public Optional<ArrendatarioM> findById(Long id);

    @Query(fields = "{'nombre','apPaterno', 'apMaterno','edad', 'telefono','fechaInicio','fechaFin'}")
    public Iterable<ArrendatarioM> findAll();

    public long count();

}
