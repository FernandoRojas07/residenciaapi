package com.develop.servicios.controller;

import com.develop.servicios.Document.ArrendatarioM;
import com.develop.servicios.service.ArrendatarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/arrendatarios")
public class ArrendatarioC {

    @Autowired
    private ArrendatarioService arrenatarioService;

    @PostMapping
    public ResponseEntity<?> create(@RequestBody ArrendatarioM arrendatarioM){
        return ResponseEntity.status(HttpStatus.CREATED).body(arrenatarioService.save(arrendatarioM));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> read(@PathVariable(value = "id") Long arrendatarioId){
        Optional<ArrendatarioM> oArrendatario = arrenatarioService.findById(arrendatarioId);

        if(!oArrendatario.isPresent()){
            return ResponseEntity.notFound().build();
        }
        return  ResponseEntity.ok(oArrendatario);
    }

    @GetMapping("/getArrendatarios")
    public ResponseEntity<?> readAll(){
    Iterable<ArrendatarioM> IArrendatarios = arrenatarioService.findAll();
    return ResponseEntity.ok(IArrendatarios);
    }


}
