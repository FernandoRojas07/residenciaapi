package com.develop.servicios.Document;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.validation.constraints.NotNull;
import java.io.Serializable;


@Document(collection = "Arrendatarios")
public class ArrendatarioM implements  Serializable{

    @Id
    @NotNull
    private String curpArrendatario;
    @Field
    private String nombreArrendatario;
    @Field
    private String apPatArrendatario;
    @Field
    private String apMatArrendatario;
    @Field
    private String telefonoArrendatario;
    @Field
    private int edadArrendatario;
    @Field
    private String fechaInicio;
    @Field
    private String fechaFin;


    public ArrendatarioM() {
    }

    public ArrendatarioM(String curpArrendatario, String nombreArrendatario, String apPatArrendatario, String apMatArrendatario, String telefonoArrendatario, int edadArrendatario, String fechaInicio, String fechaFin) {
        this.curpArrendatario = curpArrendatario;
        this.nombreArrendatario = nombreArrendatario;
        this.apPatArrendatario = apPatArrendatario;
        this.apMatArrendatario = apMatArrendatario;
        this.telefonoArrendatario = telefonoArrendatario;
        this.edadArrendatario = edadArrendatario;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
    }



    public String getCurpArrendatario() {
        return curpArrendatario;
    }

    public void setCurpArrendatario(String curpArrendatario) {
        this.curpArrendatario = curpArrendatario;
    }

    public String getNombreArrendatario() {
        return nombreArrendatario;
    }

    public void setNombreArrendatario(String nombreArrendatario) {
        this.nombreArrendatario = nombreArrendatario;
    }

    public String getApPatArrendatario() {
        return apPatArrendatario;
    }

    public void setApPatArrendatario(String apPatArrendatario) {
        this.apPatArrendatario = apPatArrendatario;
    }

    public String getApMatArrendatario() {
        return apMatArrendatario;
    }

    public void setApMatArrendatario(String apMatArrendatario) {
        this.apMatArrendatario = apMatArrendatario;
    }

    public String getTelefonoArrendatario() {
        return telefonoArrendatario;
    }

    public void setTelefonoArrendatario(String telefonoArrendatario) {
        this.telefonoArrendatario = telefonoArrendatario;
    }

    public int getEdadArrendatario() {
        return edadArrendatario;
    }

    public void setEdadArrendatario(int edadArrendatario) {
        this.edadArrendatario = edadArrendatario;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    @Override
    public String toString() {
        return "ArrendatarioM{"  +
                ", curpArrendatario='" + curpArrendatario + '\'' +
                ", nombreArrendatario='" + nombreArrendatario + '\'' +
                ", apPatArrendatario='" + apPatArrendatario + '\'' +
                ", apMatArrendatario='" + apMatArrendatario + '\'' +
                ", telefonoArrendatario='" + telefonoArrendatario + '\'' +
                ", edadArrendatario=" + edadArrendatario +
                ", fechaInicio='" + fechaInicio + '\'' +
                ", fechaFin='" + fechaFin + '\'' +
                '}';
    }
}
