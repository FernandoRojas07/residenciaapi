package com.develop.servicios;

import com.develop.servicios.Document.ArrendatarioM;
import com.develop.servicios.repository.ArrendatarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class ServiciosApplication implements CommandLineRunner {

	@Autowired
	ArrendatarioRepository arrendatarioRepository;
	public static void main(String[] args) {
		SpringApplication.run(ServiciosApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		showAllGroceryItems();

	}
	public void showAllGroceryItems() {

		arrendatarioRepository.findAll().forEach(item -> System.out.println(getItemDetails(item)));
	}
	public String getItemDetails(ArrendatarioM item) {

		System.out.println(item.toString()
		);

		return "";
	}
}
